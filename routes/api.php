<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;




/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('services', [ServiceController::class, 'service']);
Route::get('subservices', [ServiceController::class, 'subservices']);
Route::get('subLocations', [LocationController::class, 'subLocations']);
Route::post('login', [AuthController::class, 'login']);
Route::post('google_login', [AuthController::class, 'googleLogin']);
Route::post('apple_login', [AuthController::class, 'appleLogin']);
Route::post('facebook_login', [AuthController::class, 'facebookLogin']);
Route::get('locations', [LocationController::class, 'location']);
Route::post('register', [AuthController::class, 'register']);
Route::get('filterPost', [PostController::class, 'filterPost'],['']);
Route::get('get_post_comment', [PostController::class, 'getPostComment']);
Route::get('get_post_reviews', [PostController::class, 'getPostReviews']);
Route::get('post_detail', [PostController::class, 'postDetail']);
//Users
Route::post('user_register', [UserAuthController::class, 'register']);
Route::post('user_login', [UserAuthController::class, 'login']);
Route::get('user_details', [UserAuthController::class, 'userDetails']);
Route::get('service_details', [UserAuthController::class, 'serviceDetails']);
Route::post('service_users', [ServiceController::class, 'serviceUsers']);
Route::post('service_provider_comment', [PostController::class, 'serviceProviderComment']);
Route::get('get_service_reviews', [ServiceController::class, 'getServiceUsers']);


Route::middleware('auth:api')->group(function () 
{

	Route::get('deleteUser', [AuthController::class, 'deleteUser']);
	Route::post('addPost', [PostController::class, 'addPost']);
	Route::post('add_comments', [PostController::class, 'addComments']);
	Route::post('add_reviews', [PostController::class, 'addReviews']);
	Route::post('updatePost', [PostController::class, 'updatePost']);
	Route::post('update_profile', [ServiceController::class, 'updateProfile']);
	Route::get('getUsers', [UsersController::class, 'getUsers']);
	Route::get('posts', [PostController::class, 'posts']);
	Route::get('getPost', [PostController::class, 'getPost']);
	//Route::get('filterPost', [PostController::class, 'filterPost']);
	Route::get('deletePost', [PostController::class, 'deletePost']);
	Route::get('get_profile', [ServiceController::class, 'get_profile']);
	Route::post('service_reviews', [ServiceController::class, 'addServiceReviews']);

});






