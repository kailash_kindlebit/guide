<?php
namespace App\Helpers;

class ApiResponse {
  
  public static function success($message, $data = [])
  {

    return response()->json([
        'success' => true,
        'data'    =>  $data,
        'message' => $message,
      ]);
  }

  public static function error($message, $data = [])
  {
    
      return response()->json([
        'success' => false,
        'message' => $message,
        'errors'  => $data
      ]);
  }

}
