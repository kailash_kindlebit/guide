<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\Post;

class Post extends Model
{
    protected $table = 'post';
     protected $fillable = [
        'id',
        'type',
        'title',
        'description',
        'image',
        'service',
        'login',
        'created_at',
        'updated_at'

    ];

}
