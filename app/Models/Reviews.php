<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;

class Reviews extends Model
{
    protected $table = 'post_reviews';
     protected $fillable = [
        'id',
        'login',
        'post_id',
        'reviews',
        'rating',
        'created_at',
        'updated_at'

    ];

}
