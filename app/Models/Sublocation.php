<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\User;

class Sublocation extends Model
{
    protected $table = 'sub_locations';
     protected $fillable = [
        'id',
        'location_id',
        'name',
        'name_ar',
        'image',
        'status',

    ];

}
