<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\Post;

class Comments extends Model
{
    protected $table = 'post_comment';
     protected $fillable = [
        'id',
        'login',
        'post_id',
        'comment',
        'created_at',
        'updated_at'

    ];

}
