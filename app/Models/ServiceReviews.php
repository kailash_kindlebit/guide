<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;

class ServiceReviews extends Model
{
    protected $table = 'service_reviews';
     protected $fillable = [
        'id',
        'login',
        'service_id',
        'rating',
        'reviews',
        'created_at',
        'updated_at'

    ];

}
