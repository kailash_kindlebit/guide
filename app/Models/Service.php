<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\User;

class Service extends Model
{
    protected $table = 'services';
     protected $fillable = [
        'id',
        'name',
        'name_ar',
        'status',

    ];

}
