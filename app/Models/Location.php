<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\User;

class Location extends Model
{
    protected $table = 'locations';
     protected $fillable = [
        'id',
        'name',
        'name_ar',
        'image',
        'status',

    ];

}
