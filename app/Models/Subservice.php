<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\User;

class Subservice extends Model
{
    protected $table = 'sub_services';
     protected $fillable = [
        'id',
        'service_id',
        'name',
        'name_ar',
        'image',
        'status',

    ];

}
