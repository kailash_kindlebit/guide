<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Subservice;
use App\Models\Service;

class SubServiceController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Sub-Services';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Subservice);

        $grid->column('id', __('ID'))->sortable();
        $grid->column('service_id', __('Service Name'))->display(function(){
          $data = Service::where('id',$this->service_id)->first();
          return $data->name;
        });

        $grid->column('name', __('Name'));
        $grid->column('name_ar', __('Arabic'));
         $grid->column('image', __('Service Image'))->image('',40,40);
        $states = [
            'on' => ['value' => '1', 'text' => 'Active', 'color' => 'success'],
            'off' => ['value' => '0', 'text' => 'Inactive', 'color' => 'danger'],
        ];
        $grid->column('status')->switch($states);
        $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Subservice::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('name', __('Name'));
        $show->field('name_ar', __('Arabic'));
        $grid->field('image', __('Service Image'))->image('',40,40);
        $show->field('status', __('Status'))->using(['1' => 'Active','0' => 'Inactive'])->label();
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
      * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Subservice);

        $form->text('id', __('ID'));
        $form->select('service_id', __('Services'))->options(Service::pluck('name','id'))->rules('required');
        $form->text('name', __('Name'));
        $form->text('name_ar', __('Arabic'));
        $form->image('image', __('Service Image'))->removable();
       $form->select('status', __('Status'))->options(['1' => 'Active', '0' => 'Inactive'])->default('1');

        return $form;
    }
}
