<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Category;

class CategoryController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Category';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Category);

        $grid->column('id', __('ID'))->sortable();
        $grid->column('name', __('Name'));
        $grid->column('image', __('Image'))->image('',40,40);
        $states = [
            'on' => ['value' => '1', 'text' => 'Active', 'color' => 'success'],
            'off' => ['value' => '0', 'text' => 'Inactive', 'color' => 'danger'],
        ];
        $grid->column('status')->switch($states);
        $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));
        $grid->disableActions();
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Post::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('name', __('Name'));
        $show->field('status', __('Status'))->using(['1' => 'Active','0' => 'Inactive'])->label();
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new LoPostcation);

        $form->text('id', __('ID'));
        $form->text('name', __('Name'));
         $form->image('image', __('Image'))->removable();
        $form->select('status', __('Status'))->options(['1' => 'Active', '0' => 'Inactive'])->default('1');

        return $form;
    }
}
