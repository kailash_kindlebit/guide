<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Sublocation;
use App\Models\Service;
use App\Models\Location;

class SubLoctionController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Sub-Locations';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Sublocation);

        $grid->column('id', __('ID'))->sortable();
        $grid->column('location_id', __('Location Name'))->display(function(){
          $data = Location::where('id',$this->location_id)->first();
          return $data->name;
        });

        $grid->column('name', __('Name'));
        $grid->column('name_ar', __('Arabic'));
         $grid->column('image', __('Location Image'))->image('',40,40);
        $states = [
            'on' => ['value' => '1', 'text' => 'Active', 'color' => 'success'],
            'off' => ['value' => '0', 'text' => 'Inactive', 'color' => 'danger'],
        ];
        $grid->column('status')->switch($states);
        $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Sublocation::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('name', __('Name'));
        $show->field('name_ar', __('Arabic'));
        $grid->field('image', __('Location Image'))->image('',40,40);
        $show->field('status', __('Status'))->using(['1' => 'Active','0' => 'Inactive'])->label();
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
      * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Sublocation);

        $form->text('id', __('ID'));
        $form->select('location_id', __('Location'))->options(Location::pluck('name','id'))->rules('required');
        $form->text('name', __('Name'));
        $form->text('name_ar', __('Arabic'));
        $form->image('image', __('Location Image'))->removable();
       $form->select('status', __('Status'))->options(['1' => 'Active', '0' => 'Inactive'])->default('1');

        return $form;
    }
}
