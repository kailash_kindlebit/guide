<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use App\Models\User;
use App\Models\Post;
use App\Models\Service;
use App\Models\Location;
use App\Models\Subservice;

class HomeController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->title('Dashboard')
            ->description('Description...');
    }
        public function home() 
    {
   
        // //Current Date Appointments
        // $startDate = Carbon::today();

        // $today  = Appointment::whereDate('appointment_date', $startDate)
        // ->where('status',1)
        // ->get()
        // ->count();


        // //Next 7 days Appointments
        // $endDate = Carbon::today()->addDays(7);

        // $nextweek = Appointment::whereBetween('appointment_date', [$startDate, $endDate])
        // ->where('status',1)
        // ->get()
        // ->count();


        // //Next 1 Month Appointments
        // $nextmonthDate = Carbon::today()->addDays(30);

        // $nextmonth = Appointment::whereBetween('appointment_date', [$startDate, $nextmonthDate])
        // ->where('status',1)
        // ->get()
        // ->count();      

        // //Last week earnings 

        // $lastweekearnings  = Transaction::where('booking_fee_status', 'succeeded')
        // ->whereDate('created_at','>=',Carbon::now()->subDays(7))
        // ->sum('transaction_amount');

        // //Last Month earnings 

        // $lastmonthearnings  =  Transaction::where('booking_fee_status', 'succeeded')
        // ->whereDate('created_at', '>=', Carbon::now()->subDays(30))
        // ->sum('transaction_amount'); 

        // //Total earnings  

        // $totalearnings  = Transaction::where('booking_fee_status', 'succeeded')
        // ->sum('transaction_amount');


        //Total Registered Doctor count
         $totalserviceprovider = User::where('type', '=', 0)
         ->where('status',1)
         ->get()
         ->count();  

        //Total Registered Patient count

         $totaluser = User::where('type', '=', 1)
         ->where('status',1)
         ->get()
         ->count();  

        $totalpost = Post::all()->count();  
        $totalsubservice = Subservice::all()->count();  
        $totallocation = Location::all()->count();  
        $totalservice = Service::all()->count();  

          return view('admin/dashboard-content',compact('totalserviceprovider','totaluser','totalpost','totalsubservice','totallocation','totalservice'));
    }

}
