<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@home')->name('home');
    $router->resource('sub-services', SubServiceController::class);
    $router->resource('sub-locations', SubLoctionController::class);
    $router->resource('services', ServiceController::class);
    $router->resource('services-provider', ServiceProviderController::class);
    $router->resource('locations', LocationController::class);
    $router->resource('post', PostController::class);
});