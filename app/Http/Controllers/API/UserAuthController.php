<?php
namespace App\Http\Controllers\API;

use DB;
use JWTAuth;
use Validator;
use JWTAuthException;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Helpers\ApiResponse;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserServiceResource;
use App\Http\Resources\UserNewServiceResource;
use App\Models\Service;
use App\Models\Post;
use App\Http\Resources\PostResource;

class UserAuthController extends Controller
{
	public function register(request $request)
    {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'name'      => 'required|string|max:255',
            'email'     => 'required|string|email|max:255|unique:users',
            'password' => 'required|min:6',
        ]);
      
        if($validator->fails())
        {
            return response()->json(['status' => false, 'error' => $validator->errors()],400);
        }
        $user = new User;
        $token = null;
        //$token = JWTAuth::parseToken()->authenticate();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->type = '1';
        $user->save();
         if (!$token = JWTAuth::attempt($request->only('email', 'password'))) {
              //return ApiResponse::success('success','invalid login');
            }else{
                $user = auth()->user()->id;
                User::where('id',$user)->update(['remember_token' => $token]);
                $token =  User::where('id',$user)->first();
                $token = $token->remember_token;
            }
          $users = auth()->user();
      	  $user = new UserResource($users, $token);
          return ApiResponse::success('Successfully registered',$user);                          
    }


    public function login(Request $request) {    
        
     $validator = Validator::make($request->all(), [
          'email' => 'required|email',
          'password' => 'required|min:6',
       ]);

      if($validator->fails()) {
          $error = $validator->messages()->first();
          return ApiResponse::success($error);
      }  
        // $credentials = $request->validated();
        
      $token = null;

      try
       {
            if (!$token = JWTAuth::attempt($request->only('email', 'password'))) {
              return ApiResponse::error('success','invalid login');
            }
        } catch (JWTAuthException $e)

        {
           return ApiResponse::success('success','invalid token');
        }
        
        $user = $request->user();


        if($user->status != '1'){
           return ApiResponse::error('your account has been suspended');
        }
        // if(empty($user->email_verified_at)){
        //    return ApiResponse::error('your account is not verified, please verify your account');
        // }
        User::where('id',$user->id)->update(['remember_token' => $token]);

        $user = new UserResource($user, $token);
        // return $this->sendResponse('Success', $user);
        return ApiResponse::success('succes',$user);
    }

    public function googleLogin(Request $request){
        $validator = Validator::make($request->all(), [
           'name'      => 'required|string|max:255',
           'email'     => 'string|email',
           'google_id' => 'required|string',
           'device_type'  => 'string',
        ]);

        if($validator->fails()) {
          $error = $validator->messages()->first();
          return ApiResponse::success($error);
        }  
        $data = $request->validated();

        $data = $request->only(['name','email','google_id', 'device_type']);

        $data['password'] = bcrypt($data['google_id']);
        $user = User::firstOrCreate(['google_id' => $request->google_id], $data); 
        if($user) {
             if(!$user->google_id){
                $user->google_id = $request->google_id;
                $user->save();
             }
             try {
                if (! $token = JWTAuth::fromUser($user)) {
                  return ApiResponse::error('invalid_credentials');
                }
                 
             } catch (JWTException $e) {
                 return ApiResponse::error('could_not_create_token');
             }
        }
        User::where('id',$user->id)->update(['remember_token' => $token]);
        $user = new UserResource($user, $token);

        return ApiResponse::success('succes',$user);
    }

    public function userDetails(Request $request){
        $data = $request->all();
        $users = User::where('id',$data['id'])->get();
        $data =  UserNewServiceResource::collection($users);
        return ApiResponse::success('succes',$data);
    }

    public function serviceDetails(Request $request){
        $data = $request->all();
        $details = [];
        $service = Service::where('id',$data['id'])->get();
        if(count($service)!=0){
            $details['name']= $service[0]->name;
            $details['name_ar']= $service[0]->name_ar;
            $details['service_image']= asset('/').'upload'.$service[0]->image;
            $posts = Post::where('login',$data['id'])->get();
            $p = PostResource::collection($posts);
            $details['posts']= $p;
            //print_r($details);
        }
       
        return ApiResponse::success('succes',$details);

    }

}