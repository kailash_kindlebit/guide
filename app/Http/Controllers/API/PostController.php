<?php
namespace App\Http\Controllers\API;
use DB;
use JWTAuth;
use Validator;
use JWTAuthException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\User;
use App\Helpers\ApiResponse;
use Illuminate\Support\Facades\Auth;
use App\Models\Service;
use App\Models\Subservice;
use App\Models\Comments;
use App\Models\Reviews;
use App\Http\Resources\PostResource;
use App\Http\Resources\PostCommentResource;
use App\Http\Resources\PostDetailsResource;
use App\Http\Resources\PostReviewsResource;
use App\Http\Resources\PostDataResource;


class PostController extends Controller
{   
    public function addPost(Request $request) { 
    	$user = auth()->user();
		$validator = Validator::make($request->all(), [
            'title' => 'required',
            // 'type' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);
        $data = $request->all();

        if($validator->fails())
        {
            return response()->json(['status' => false, 'error' => $validator->errors()],400);
        } else {
        	$type = Service::where('id',$user->category)->first();
        	$post = new Post;
        	$post->title = $request->title;
	        $post->type = $type->id;
	        $post->description = $request->description;
	        $post->service = $user->category;
	        $post->user_location = $user->location;
	        $post->login = $user->id;
	        $post->status = '1';
	        //$post->image = 'test.png';

	        if($request->hasFile('image')) {
				foreach($request->file('image') as $key=>$file)
				{
					$image = 'services'.$key.time().'.'.$file->getClientOriginalExtension();
					$file->storeAs('public/upload/post', $image);  
					$img[] = '/post/'.$image;    
				}
			    $post->image = implode(',',$img);     
            }
	        $post->save();
	        return ApiResponse::success('Success',"Post added successfully"); 
        }
    }
    public function allposts(Request $request) {
    	
		$posts = Post::where('status',1)->orderBy('created_at', 'desc')->paginate(5)->appends(request()->query());
		
		return PostResource::collection($posts);
    }
    
    public function posts(Request $request) {
    	$user = auth()->user()->id;
    	$data = $request->all();
    	$location = $data['location_id'];
		$posts = Post::where('user_location',$location)->orderBy('created_at', 'desc')->get()->toArray();
		$data = [];
		if(count($posts)!=0){
			foreach($posts as $key=>$post){
				$postUser = User::find($post['login']);
				$imgs = explode(',',$post['image']);
				$images= [];
				foreach($imgs as $keys=>$img){
					$images[$keys] = asset('storage/upload/').$img;
				}
				$data[$key]['id'] = $post['id'];
				$data[$key]['user'] = $postUser->name;
                if($postUser->profile){
                    $imageP = asset('storage/upload').$postUser->profile;
                }else{
                    $imageP = asset('storage/upload/images/profile.PNG');
                }
                $reviews = Reviews::where('post_id',$post['id'])->get();
                $reviewsSum = Reviews::where('post_id',$post['id'])->get()->sum('rating');
                $totalcomment= count($reviews);
                if($totalcomment != 0){
		          $avg = $reviewsSum / $totalcomment;
		        }else{
		          $avg = 0;
		        }

				$data[$key]['total_review'] = $totalcomment;
				$data[$key]['rating'] = (int) round(($avg), 0);
				$data[$key]['user_profile'] = $imageP;
				$data[$key]['mobile_number'] = $postUser->phone;
				$data[$key]['id'] = $post['id'];
				$data[$key]['title'] = $post['title'];
				$data[$key]['description'] = $post['description'];
				$data[$key]['image'] = $images[0];
				$data[$key]['login'] = $user;
				$service_name = Service::where('id',$postUser['category'])->first();
				$data[$key]['service_name'] =  $service_name->name;
				$category = Subservice::where('id',$postUser['sub_category_id'])->first();
				$data[$key]['category'] =  $category->name;
				$data[$key]['status'] = $post['id'];
				$data[$key]['created_at'] = $post['created_at'];
			}
		}
		
		return ApiResponse::success('success',$data);
    }
    public function getPost(Request $request) { 
    	$user = auth()->user()->id;
    	$data = $request->all();
    	$id = $data['id'];
		$post = Post::where('id',$id)->get();
		return PostDataResource::collection($post);
		$data = [];
		if(count($post)!=0){
			$imgs = explode(',',$post[0]['image']);
			$images= [];
			foreach($imgs as $key=>$img){
				$images[$key] = asset('storage/upload/').$img;
			}
			$profile = asset('storage/upload').auth()->user()->profile;
	        if(auth()->user()->profile == ''){
	            $profile = asset('storage/upload/images/profile.PNG');
	        }
			
			$data['id'] = $post[0]['id'];
			$data['title'] = $post[0]['title'];
			$data['user'] = auth()->user()->name;
			$data['user_profile'] = $profile;
			$data['description'] = $post[0]['description'];
			$data['image'] = $images;
			$data['login'] = $user;
			$service_name = Service::where('id',auth()->user()->category)->first();
			$data['service_name'] =  $service_name->name;
			$category = Subservice::where('id',auth()->user()->sub_category_id)->first();
			$data['category'] =  $category->name;
			$data['status'] = $post[0]['id'];
			$data['created_at'] = $post[0]['created_at'];
		}
		
		return ApiResponse::success('success',$data);
    }
    public function filterPost(Request $request) {
    	
    	$data = $request->all();
    	$service = $data['service'];
    	$location = $data['location'];
    	$posts = Post::select('post.*','users.name','users.profile','users.category','users.sub_category_id','users.id as user_id','users.location as location')->leftJoin('users', 'post.login', '=', 'users.id')->where('users.location',$location);   
    	if($service =='all'){
    		$posts->where('post.status',1);
    	}else{

    		if($data['service'] !='all'){
    			$service = $data['service'];
    		    $posts->where('post.service',$service)->where('post.status',1);
    		}
    		// if($data['sub_service'] !=''){
    		// 	$sub_service = $data['sub_service'];
    		//     $posts->where('users.sub_category_id',$sub_service)->where('post.status',1);
    		// }
    		
    	}

    	$data = $posts->orderBy('post.created_at', 'desc')->paginate(5)->appends(request()->query());
    	return PostResource::collection($data);
    	
		
	}
	public function deletePost(Request $request) { 
	    	$data = $request->all();
	    	$user = auth()->user()->id;
	    	$id = $data['id'];
			$post = Post::where('login',$user)->where('id',$id)->delete();
			return ApiResponse::success('Deleted successfully');
	}

    public function updatePost(Request $request) { 
		$user = auth()->user();
		$validator = Validator::make($request->all(), [
            'title' => 'required',
            // 'type' => 'required',
            'description' => 'required',
            //'image' => 'required'
        ]);
        $id = $request->id;

        if($validator->fails())
        {
            return response()->json(['status' => false, 'error' => $validator->errors()],400);
        } else {
        	$post = new Post;
        	$type = Service::where('id',$user->category)->first();
        	$post['title'] = $request->title;
	        $post['type'] = $type->id;
	        $post['description'] = $request->description;
	        $post['login'] = $user->id;
	        $post['status'] = '1';
	        //$post['image'] = 'images.img';
	        
            if($request->hasFile('image')) {
				foreach($request->file('image') as $key=>$file)
				{
					$image = 'services'.$key.time().'.'.$file->getClientOriginalExtension();
					$file->storeAs('public/upload/post', $image);  
					$img[] = '/post/'.$image;    
				}
			    $post['image'] = implode(',',$img);     
            }
            $details = $post->toArray(); 
	        Post::where('id',$id)->update($details);
	        return ApiResponse::success('Success',"Post updated successfully"); 
        }
    }
    public function addComments(Request $request) { 
    	$user = auth()->user();
		$validator = Validator::make($request->all(), [
            'post_id' => 'required',
            'comment' => 'required'
        ]);
        $data = $request->all();

        if($validator->fails())
        {
            return response()->json(['status' => false, 'error' => $validator->errors()],400);
        } else {
        	
        	$comments = new Comments;
        	$comments->comment = $request->comment;
        	$comments->post_id = $request->post_id;
	        $comments->login = $user->id;;
	        $comments->save();
	        return ApiResponse::success('Success',"Comment added successfully"); 
        }
    }

    public function getPostComment(Request $request) {
    	$data = $request->all();
		$comments = Comments::where('post_id',$data['post_id'])->orderBy('created_at', 'desc')->paginate(1)->appends(request()->query());

		return PostCommentResource::collection($comments);
    }

    public function addReviews(Request $request) { 
    	$user = auth()->user();
		$validator = Validator::make($request->all(), [
            'post_id' => 'required',
            'reviews' => 'required',
            'rating' => 'required'
        ]);
        $data = $request->all();

        if($validator->fails())
        {
            return response()->json(['status' => false, 'error' => $validator->errors()],400);
        } else {
        	$postData = Post::find($request->post_id);
        	$reviews = new Reviews;
        	$reviews->reviews = $request->reviews;
        	$reviews->post_id = $request->post_id;
        	$reviews->rating = $request->rating;
	        $reviews->login = $user->id;;
            $reviews->service_provider_id = $postData->login;;
	        $reviews->save();
	        return ApiResponse::success('Success',"Reviews added successfully"); 
        }
    }

    public function getPostReviews(Request $request) {
    	$data = $request->all();
    	// $reviews = Reviews::where('post_id',$data['post_id'])->orderBy('created_at', 'desc')->first();
    	// echo $reviews->created_at->diffForHumans(); // a second ago
    	// die;
		$reviews = Reviews::where('post_id',$data['post_id'])->orderBy('created_at', 'desc')->paginate(4)->appends(request()->query());

		return PostReviewsResource::collection($reviews);
    }
     public function postDetail(Request $request) { 
    	$data = $request->all();
    	$post_id = $data['post_id'];
		$post = Post::where('id',$post_id)->get();
		$posts =[];
		
		$data =  PostDetailsResource::collection($post);
		
		$posts['data'] = $data;
		return $posts;
    }

    public function serviceProviderComment(Request $request) { 
    	$servie_provider = Reviews::where('service_provider_id',$request->id)->get();
    	$data = PostCommentResource::collection($servie_provider);
    	return ApiResponse::success('Success',$data);  
    }

}