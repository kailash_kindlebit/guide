<?php
namespace App\Http\Controllers\API;

use DB;
use JWTAuth;
use Validator;
use JWTAuthException;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Post;
use App\Helpers\ApiResponse;
use App\Http\Resources\UserResource;

class AuthController extends Controller
{
	public function register(RegisterRequest $request)
    { 
        $data = $request->all();
        $user = new User;
        $token = null;
        //$token = JWTAuth::parseToken()->authenticate();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->phone = $request->phone;
        $user->service_name = $request->service_name;
        $user->category = $request->category;
        $user->location = $request->location;
        $user->city = $request->city;
        $user->sub_category_id = $request->sub_category_id;
        $user->type = '0';
        if($request->hasFile('service_image')) {
            
			foreach($request->file('service_image') as $key=>$file)
			{

				$image = 'services'.$key.time().'.'.$file->getClientOriginalExtension();
				$file->storeAs('public/upload/service-image', $image);  
				$img[] = '/service-image/'.$image;    
			}
			$user->service_image = implode(',',$img);     
        }

        if($request->hasFile('service_document')) {
            $file = $data['service_document'];

			$image = 'document'.time().'.'.$file->getClientOriginalExtension();
			$file->storeAs('public/upload/service-document', $image); 
			$user->service_document = '/service-document/'.$image;     
        }

        if($request->hasFile('profile')) {
            $file = $data['profile'];

            $image = 'document'.time().'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/upload/profile', $image); 
            $user->profile = '/profile/'.$image;     
        }

        $user->save();
         if (!$token = JWTAuth::attempt($request->only('email', 'password'))) {
              //return ApiResponse::success('success','invalid login');
            }else{
                $user = auth()->user()->id;
                User::where('id',$user)->update(['remember_token' => $token]);
                $token =  User::where('id',$user)->first();
                $token = $token->remember_token;
            }
          $users = auth()->user();
      	  $user = new UserResource($users, $token);
         // return $this->sendResponse('Success', $user);
          return ApiResponse::success('Successfully registered',$user);                          
    }


    public function login(Request $request) {    
     $validator = Validator::make($request->all(), [
          'email' => 'required|email',
          'password' => 'required|min:6',
       ]);

      if($validator->fails()) {
          $error = $validator->messages()->first();
          return ApiResponse::success($error);
      }  
        // $credentials = $request->validated();
        
      $token = null;

      try
       {
            if (!$token = JWTAuth::attempt($request->only('email', 'password'))) {
              return ApiResponse::error('success','invalid login');
            }
        } catch (JWTAuthException $e)

        {
           return ApiResponse::success('success','invalid token');
        }
        
        $user = $request->user();


        if($user->status != '1'){
           return ApiResponse::error('your account has been suspended');
        }
        // if(empty($user->email_verified_at)){
        //    return ApiResponse::error('your account is not verified, please verify your account');
        // }
        User::where('id',$user->id)->update(['remember_token' => $token]);

        $user = new UserResource($user, $token);
        // return $this->sendResponse('Success', $user);
        return ApiResponse::success('succes',$user);
    }

    public function googleLogin(Request $request){
        $validator = Validator::make($request->all(), [
           'name'      => 'required|string|max:255',
           'email'     => 'string|email',
           'google_id' => 'required|string',
           'device_type'  => 'string',
        ]);

        if($validator->fails()) {
          $error = $validator->messages()->first();
          return ApiResponse::error($error);
        }  
        $checkUser = User::where('email', $request->email)->get();
        if(count($checkUser)!=0){
            $token = null;
            $email = $request->email;
            $password = $request->google_id;
            try
           {
                if (!$token = JWTAuth::attempt(['email' => $email,'password' => $password])) {
                  return ApiResponse::error('success','invalid login');
                }
            } catch (JWTAuthException $e)

            {
               return ApiResponse::success('success','invalid token');
            }
             $user = $request->user();
             if($user->status != '1'){
                   return ApiResponse::error('your account has been suspended');
                }
            User::where('id',$user->id)->update(['remember_token' => $token]);

            $user = new UserResource($user, $token);
             // return $this->sendResponse('Success', $user);
            return ApiResponse::success('succes',$user);    
        
        }

        $data = $request->only(['name','email','google_id', 'device_type']);

        $data['password'] = bcrypt($data['google_id']);
        $data['type'] = 1;
        $user = User::firstOrCreate(['google_id' => $request->google_id], $data); 
        if($user) {
             if(!$user->google_id){
                $user->google_id = $request->google_id;
                $user->save();
             }
             try {
                if (! $token = JWTAuth::fromUser($user)) {
                  return ApiResponse::error('invalid_credentials');
                }
                 
             } catch (JWTException $e) {
                 return ApiResponse::error('could_not_create_token');
             }
        }
        User::where('id',$user->id)->update(['remember_token' => $token,'login_type'=>'1']);
        $user = new UserResource($user, $token);

        return ApiResponse::success('succes',$user);
    }


        public function appleLogin(Request $request){
       $validator = Validator::make($request->all(), [
           'apple_id' => 'required|string',
        ]);

        if($validator->fails()) {
          $error = $validator->messages()->first();
          return ApiResponse::error($error);
        }  
        $checkUser = User::where('apple_id', $request->apple_id)->get();
        if(count($checkUser)!=0){
            $token = null;
            $email =  $request->apple_id.'@gmail.com';
            $password = $request->apple_id;
            try
           {
                if (!$token = JWTAuth::attempt(['email' => $email,'password' => $password])) {
                  return ApiResponse::error('success','invalid login');
                }
            } catch (JWTAuthException $e)

            {
               return ApiResponse::success('success','invalid token');
            }
             $user = $request->user();
             if($user->status != '1'){
                   return ApiResponse::error('your account has been suspended');
                }
            User::where('id',$user->id)->update(['remember_token' => $token]);

            $user = new UserResource($user, $token);
             // return $this->sendResponse('Success', $user);
            return ApiResponse::success('success',$user);    
        
        }

        $data['apple_id'] = $request->apple_id;
        $data['email'] = $request->apple_id.'@gmail.com';
        $data['name'] = $request->apple_id;
        $data['device_type'] = 'ios';
        $data['password'] = bcrypt($data['apple_id']);
        $data['type'] = '1';
        $user = User::firstOrCreate(['apple_id' => $request->apple_id], $data); 
        if($user) {
             if(!$user->apple_id){
                $user->apple_id = $request->apple_id;
                $user->save();
             }
             try {
                if (! $token = JWTAuth::fromUser($user)) {
                  return ApiResponse::error('invalid_credentials');
                }
                 
             } catch (JWTException $e) {
                 return ApiResponse::error('could_not_create_token');
             }
        }
        User::where('id',$user->id)->update(['remember_token' => $token,'login_type'=>'1']);
        $user = new UserResource($user, $token);

        return ApiResponse::success('succes',$user);
    }

    public function facebookLogin(Request $request){
        $validator = Validator::make($request->all(), [
           'name'      => 'required|string|max:255',
           'email'     => 'string|email',
           'google_id' => 'required|string',
           'device_type'  => 'string',
        ]);

        if($validator->fails()) {
          $error = $validator->messages()->first();
          return ApiResponse::success($error);
        }  
        $checkUser = User::where('email', $request->email)->get();
        if(count($checkUser)!=0){
            $checkUser = User::where('email', $request->email)->delete();
        }
        $data = $request->only(['name','email','google_id', 'device_type']);

        $data['password'] = bcrypt($data['google_id']);
        $data['type'] = 1;
        $user = User::firstOrCreate(['google_id' => $request->google_id], $data); 
        if($user) {
             if(!$user->google_id){
                $user->google_id = $request->google_id;
                $user->save();
             }
             try {
                if (! $token = JWTAuth::fromUser($user)) {
                  return ApiResponse::error('invalid_credentials');
                }
                 
             } catch (JWTException $e) {
                 return ApiResponse::error('could_not_create_token');
             }
        }
        User::where('id',$user->id)->update(['remember_token' => $token,'login_type'=>'2']);
        $user = new UserResource($user, $token);

        return ApiResponse::success('succes',$user);
    }

    public function deleteUser(Request $request){
        $user_id = auth()->user();
        User::where('id', $user_id->id)->delete();
        if($user_id->type == 0){
          Post::where('login',$user_id->id)->delete();
        }
    }

}