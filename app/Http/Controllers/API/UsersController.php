<?php
namespace App\Http\Controllers\API;

use DB;
use JWTAuth;
use Validator;
use JWTAuthException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Resources\UserResource;
use App\Helpers\ApiResponse;

class UsersController extends Controller
{   
    public function getUsers(Request $request) { 
		$users = $request->user();
		return ApiResponse::success('success',new UserResource($users));
    }
}