<?php
namespace App\Http\Controllers\API;

use DB;
use JWTAuth;
use Validator;
use JWTAuthException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\LocationResource;
use App\Models\Location;
use App\Models\Sublocation;
use App\Helpers\ApiResponse;

class LocationController extends Controller
{   
    public function location(Request $request) { 
    	$data = $request->all();
    	$type = $data['type'];
    	$service = Location::where('status',1)->get();
    	if($type == 'ar'){
    		$service = Location::select('id','name_ar as name','status','image')->where('status',1)->get();
    	}
		$serviceResource = LocationResource::collection($service);
		return ApiResponse::success('success',$serviceResource);
    }

    public function subLocations(Request $request){
        $data = $request->all();
        $type = $data['type'];
        $sublocations = Sublocation::where('status',1)->where('location_id',$data['id'])->get()->toArray();
        if($type == 'ar'){
            $sublocations = Sublocation::select('id','location_id','name_ar as name','image','status','created_at','updated_at')->where('status',1)->where('location_id',$data['id'])->get()->toArray();
        }
        
        $data= [];
        foreach($sublocations as $key=>$sublocation){

        	$location_name = Location::where('id',$sublocation['location_id'])->first();
	        if($type == 'ar'){
	           $location_name = Location::select('name_ar as name')->where('id',$sublocation['location_id'])->first();
	        }

            $data[$key]['id'] = $sublocation['id'];
            
            $data[$key]['location_name'] =  $location_name->name;
            $data[$key]['name'] = $sublocation['name'];
            $data[$key]['image'] = asset('/upload/').'/'.$sublocation['image'];
            $data[$key]['status'] = $sublocation['status'];
            $data[$key]['created_at'] = $sublocation['created_at'];
        }
        return ApiResponse::success('success',$data);
    }
}