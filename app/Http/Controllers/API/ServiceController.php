<?php
namespace App\Http\Controllers\API;

use DB;
use JWTAuth;
use Validator;
use JWTAuthException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ServiceResource;
use App\Models\Service;
use App\Models\Subservice;
use App\Models\User;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserServiceResource;
use App\Http\Resources\ServiceReviewsResource;
use App\Helpers\ApiResponse;
use App\Models\Post;
use App\Models\Location;
use App\Models\Reviews;
use App\Models\ServiceReviews;

class ServiceController extends Controller
{   
    public function service(Request $request) { 
        $data = $request->all();
        $type = $data['type'];
       
		$service = Service::where('status',1)->get();
        if($type == 'ar'){
            $service = Service::select('id','name_ar as name','status','image')->where('status',1)->get();
        }
		$serviceResource = ServiceResource::collection($service);
		return ApiResponse::success('success',$serviceResource);
    }

    public function subservices(Request $request){
        $data = $request->all();
        $type = $data['type'];
        $subservices = Subservice::where('status',1)->where('service_id',$data['id'])->get()->toArray();
        if($type == 'ar'){
            $subservices = Subservice::select('id','service_id','name_ar as name','image','status','created_at','updated_at')->where('status',1)->where('service_id',$data['id'])->get()->toArray();
        }
        
        $data= [];
        foreach($subservices as $key=>$subservice){
            $data[$key]['id'] = $subservice['id'];
            $service_name = Service::where('id',$subservice['service_id'])->first();
            $data[$key]['service_name'] =  $service_name->name;
            $data[$key]['name'] = $subservice['name'];
            $data[$key]['image'] = asset('/upload/').'/'.$subservice['image'];
            $data[$key]['status'] = $subservice['status'];
            $data[$key]['created_at'] = $subservice['created_at'];
        }
        return ApiResponse::success('success',$data);
    }

    public function get_profile(Request $request){
        $user = auth()->user();
        $user_id = auth()->user()->id;
        $details = [];
        $banner_img = asset('storage/upload').$user->banner_img;
        if($user->banner_img == ''){
            $banner_img = asset('storage/upload/images/profile.PNG');
        }
        $profile = asset('storage/upload').$user->profile;
        if($user->profile == ''){
            $profile = asset('storage/upload/images/profile.PNG');
        }
        $location = Location::where('id',$user->location)->first();
        $service_names = Service::where('id',auth()->user()->category)->first();

        if($request->type == 'ar'){
          $locationName = $location->name_ar;
          $serviceName = $service_names->name_ar;
        }else{
          $locationName = $location->name;
          $serviceName = $service_names->name;
        }

        
        $serviceReviews = ServiceReviews::where('service_id',$user_id)->get();
        $totalserviceReviewsCount = count($serviceReviews);
        $reviewsSum = ServiceReviews::where('service_id',$user_id)->get()->sum('rating');
               
        if($totalserviceReviewsCount != 0){
          $serviceAvg = $reviewsSum / $totalserviceReviewsCount;
        }else{
          $serviceAvg = 0;
        }
        

        $details['banner_img'] = $banner_img;
        
        $details['total_service_review']   = $totalserviceReviewsCount;
        $details['service_review_rating']   = $serviceAvg;
        $details['profile'] = $profile;
        $details['name'] = $user->name;
        $details['phone'] = $user->phone;
        $details['location'] = $locationName;
        $details['service_names'] = $serviceName;
        $details['yourself'] = $user->yourself;
       $ser_img= [];
        if($user->service_image){
            
            $imgs = explode(',',$user->service_image);
                if(count($imgs)!=0){
                    foreach($imgs as $key=>$img){
                      $ser_img[$key] = asset('storage/upload').$img;
                    }    
                }
            
        }
        $details['service_image'] = $ser_img;
        $timeline = [];
        $posts = Post::where('login',$user_id)->limit(5)->get()->toArray();
       
        if(count($posts)!=0){
            foreach($posts as $key=>$post){
                $imgs = explode(',',$post['image']);
                $images= [];
                $profile = asset('storage/upload').auth()->user()->profile;
                if(auth()->user()->profile == ''){
                    $profile = asset('storage/upload/images/profile.PNG');
                }
                foreach($imgs as $keys=>$img){
                    $images[$keys] = asset('storage/upload').$img;
                }
                $postCommentCount = Reviews::where('post_id',$post['id'])->get();
                $reviewsSum =       Reviews::where('post_id',$post['id'])->get()->sum('rating');
         
                $totalPostCommentCount = count($postCommentCount);

                if($totalPostCommentCount != 0){
                  $avg = $reviewsSum / $totalPostCommentCount;
                }else{
                  $avg = 0;
                }

                $timeline[$key]['id'] = $post['id'];
                $timeline[$key]['totalPostCommentCount'] = $totalPostCommentCount;
                $timeline[$key]['rating'] = (int) round(($avg), 0);

                $timeline[$key]['user'] = auth()->user()->name;
                $timeline[$key]['user_profile'] = $profile;
                $timeline[$key]['id'] = $post['id'];
                $timeline[$key]['title'] = $post['title'];
                $timeline[$key]['description'] = $post['description'];
                $timeline[$key]['image'] = $images[0];
                $service_name = Service::where('id',auth()->user()->category)->first();
                $category = Subservice::where('id',auth()->user()->sub_category_id)->first();

                if($request->type == 'ar'){
                  $subserviceName = $category->name_ar;
                  $serviceName = $service_names->name_ar;
                }else{
                  $subserviceName = $category->name;
                  $serviceName = $service_names->name;
                }
                $timeline[$key]['service_name'] =  $serviceName;
                $timeline[$key]['category'] =  $subserviceName;
                $timeline[$key]['status'] = $post['id'];
                $timeline[$key]['created_at'] = $post['created_at'];
            }
        }
        //$details['user'] = $user;
        $details['timeline'] = $timeline;
        return ApiResponse::success('success',$details);
    }

    public function updateProfile(Request $request){
        $user = auth()->user()->id;
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required', 
        ]);
        $detail = [];
        if($request->hasFile('profile')) {
            $file = $data['profile'];
            $image = 'document'.time().'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/upload/profile', $image); 
            $detail['profile'] = '/profile/'.$image;     
        }
        if($request->hasFile('banner_img')) {
            $file = $data['banner_img'];
            $image = 'document'.time().'.'.$file->getClientOriginalExtension();
            $file->storeAs('public/upload/banner-img', $image); 
            $detail['banner_img'] = '/banner-img/'.$image;     
        }
        $detail['name'] = $data['name'];
        $detail['phone'] = $data['phone'];
        $detail['yourself'] = $data['yourself'];
       
        User::where('id',$user)->update($detail);
         return ApiResponse::success('Success',"updated successfully"); 

    }

    public function serviceUsers(Request $request){
        $validator = Validator::make($request->all(), [
            'service' => 'required',
            'sub_service' => 'required',
            'location' => 'required',
            'sublocation' => 'required',
            'type' => 'required'
        ]);
       
        if($validator->fails())
        {
            return response()->json(['status' => false, 'error' => $validator->errors()],400);
        }
        
        $data = $request->all();
        $users = User::where('category',$data['service'])->where('sub_category_id',$data['sub_service'])->where('location',$data['location'])->get();
        
        $serviceUsers = UserServiceResource::collection($users);

        $arr = [];
        $arr1 = [];
        $arr2 = [];
        foreach($serviceUsers as $serviceUser){
            if($data['location'] == $serviceUser->location && $data['sublocation'] == $serviceUser->city){
                
                $arr[] = $serviceUser;

            }else if($data['location'] == $serviceUser->location && $data['sublocation'] != $serviceUser->city){
               
                $arr1[] = $serviceUser;

            }else{
                $arr2[] = $serviceUser;
            }
        }
        $details = array_merge($arr,$arr1,$arr2);
      
        return ApiResponse::success('Success',$details); 

    }

    public function addServiceReviews(Request $request) { 
        $user = auth()->user();
        $validator = Validator::make($request->all(), [
            'service_id' => 'required',
            'reviews' => 'required',
            'rating' => 'required'
        ]);
        $data = $request->all();

        if($validator->fails())
        {
            return response()->json(['status' => false, 'error' => $validator->errors()],400);
        } else {
            $postData = Post::find($request->service_id);
            $reviews = new ServiceReviews;
            $reviews->reviews = $request->reviews;
            $reviews->service_id = $request->service_id;
            $reviews->rating = $request->rating;
            $reviews->login = $user->id;;
            $reviews->save();
            return ApiResponse::success('Success',"Service Reviews added successfully"); 
        }
    }

    public function getServiceUsers(Request $request) {
        $data = $request->all();
        
        $reviews = ServiceReviews::where('service_id',$data['service_id'])->orderBy('created_at', 'desc')->get();
        $reviewsCount = count($reviews);
        $reviewsSum = ServiceReviews::where('service_id',$data['service_id'])->get()->sum('rating');
        if($reviewsSum != 0){
          $avg = $reviewsSum / $reviewsCount;
        }else{
          $avg = 0;
        }
        $data = [];
        $reviews  =  ServiceReviewsResource::collection($reviews);
        $count = count($reviews);
        $data['count'] = $count;
        $data['avg'] = (int) round(($avg), 0);
        $data['reviews'] = $reviews;
        return ApiResponse::success('Success',$data); 
    }

   

}