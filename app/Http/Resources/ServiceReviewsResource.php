<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Voyager;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Reviews;
use App\Models\ServiceReviews;

class ServiceReviewsResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::where('id',$this->login)->first();
        $profile = asset('storage/upload').$user->profile;
        if($user->profile == ''){
          $profile = asset('storage/upload/images/profile.PNG');
        }
        $reviewsCount = ServiceReviews::where('service_id',$this->service_id)->get()->count();
        $reviewsSum = ServiceReviews::where('service_id',$this->service_id)->get()->sum('rating');
        if($reviewsSum != 0){
          $avg = $reviewsSum / $reviewsCount;
        }else{
          $avg = 0;
        }
         
        return [
          //'rating'  =>(int) round(($avg), 0),
          'rating'  =>$this->rating,
          'user' =>  $user->name,
          'review' =>  $this->reviews,
          'user_profile' =>  $profile,
          'time' =>  $this->created_at->diffForHumans(),
        
        ];
    }
}
;