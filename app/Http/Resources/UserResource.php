<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;
use App\Models\Service;
use App\Models\Subservice;
use App\Models\Location;

class UserResource extends JsonResource
{

    public function __construct($resource, $token = '')
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;        
        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       
        $profile = asset('storage/upload').$this->profile;
        if($this->profile == ''){
            $profile = asset('storage/upload/images/profile.PNG');
        }
        $banner_img = asset('storage/upload').$this->banner_img;
        if($this->banner_img == ''){
            $banner_img = asset('storage/upload/images/profile.PNG');
        }
        $service_name = Service::where('id',$this->category)->pluck('name');
       
        $sub_service = Subservice::where('id',$this->sub_category_id)->pluck('name');
        $location = Location::where('id',$this->location)->pluck('name');
        $imgs= '';
        $images= [];
        if($this->service_image){
             $imgs = explode(',',$this->service_image);
             if(count($imgs)!=0){
                foreach($imgs as $keys=>$img){
                 $images[$keys] = asset('storage/upload/').$img;
                }
         }
        }
        

        return [
            'token'               => $this->token,
            'id'                  => $this->id,
            'username'            => (string) $this->name,
            'email'               => (string) $this->email,
            'type'                => $this->type,
            'profile'             => $profile,
            'banner_img'          => $banner_img,
            'yourself'            => $this->yourself,
            'phone'               => $this->phone,
            'service_name'        => isset($service_name[0])?$service_name[0]:null,
            'service_id'        => $this->category,
            'sub_service'         =>isset($sub_service[0])?$sub_service[0]:null,
            'sub_service_id'         =>$this->sub_category_id,
            'location'            => isset($location[0])?$location[0]:null,
            'location_id'            => $this->location,
            'service_image'       => $images
            
        ];
    }
}
