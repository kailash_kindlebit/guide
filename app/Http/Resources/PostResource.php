<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Voyager;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Service;
use App\Models\Subservice;
use App\Models\Location;
use App\Models\Reviews;
use App\Http\Resources\UserResource;

class PostResource extends JsonResource
{

    public function toArray($request)
    {
      // return $request->type;
        $user = User::where('id',$this->login)->first();
        $user_location = Location::where('id',$user->location)->first();
       if($request->type == 'ar'){
        
        $locationName = isset($user_location->name_ar)?$user_location->name_ar:null;
           
       }else{
        $locationName = isset($user_location->name)?$user_location->name:null;
       }
        $banner_img = asset('storage/upload').$user->banner_img;
        if($user->banner_img == ''){
            $banner_img = asset('storage/upload/images/profile.PNG');
        }
        $profile = asset('storage/upload').$user->profile;
        if($user->profile == ''){
            $profile = asset('storage/upload/images/profile.PNG');
        }

        $imgs = explode(',',$this->image);
        //  print_r($imgs);
        // die;
                $images= [];
                foreach($imgs as $keys=>$img){
                    $images[$keys] = asset('storage/upload/').$img;
        }

        $uimg='';
        if(count($images)!=0){ $uimg = $images[0]; }
        if($uimg == ''){
            $uimg = asset('storage/upload/images/dummy.png');
        }

        $service_name = Service::where('id',$user->category)->first();
        if($request->type == 'ar'){

        $serviceName = isset($service_name->name_ar)?$service_name->name_ar:null;
        }else{
        $serviceName = isset($service_name->name)?$service_name->name:null;
        }
        $category = Subservice::where('id',$user->sub_category_id)->first();
        if($request->type == 'ar'){

        $categoryName = isset($category->name_ar)?$category->name_ar:null;
        }else{
        $categoryName = isset($category->name)?$category->name:null;;
        }
        $reviews = Reviews::where('post_id',$this->id)->get();
        $reviewsSum = Reviews::where('post_id',$this->id)->get()->sum('rating');
        
        $totalcomment= count($reviews);
        if($totalcomment != 0){
          $avg = $reviewsSum / $totalcomment;
        }else{
          $avg = 0;
        }
        $reviwes='';
        $reviwesUser = []; 
        if($totalcomment!=0){
            $reviwes =$reviews[0]->login;
          
            $userRe = User::where('id',$reviews[0]->login)->get();
            $reviwesUser = UserServiceResource::collection($userRe);
        }
        $rating = $avg;
        $reviews = Reviews::where('post_id',$this->id)->orderBy('created_at', 'desc')->get();
        
        $finalReview =  PostReviewsResource::collection($reviews);
        //$rating = '4.56';
         return [
          'id'            => $this->id,
          'user'          => $user->name,
          'user_id'          => $user->id,
          'user_phone'          => $user->phone,
          'user_profile'  => $profile,
          'user_location'  => $locationName,
          'title'         => $this->title,
          'description'   => $this->description,
          'first_image'         => $uimg,
          'image'         => $images,
          'service_name'  => $serviceName,
          'category'      => $categoryName,
          'status'        => $this->status,
          'created_at'    => $this->created_at,
          'time'    =>     $this->created_at->diffForHumans(),
          'reviwesUser'         => $reviwesUser,
          'post_comment'  => $finalReview,
          'review'         => $reviwes,
          'totalcomment'         => $totalcomment,
          'rating'         => (int) round(($rating), 0),

          // 'title'       => (string) $this->title,
          // 'end'         => Carbon::parse($this->end_date_time)->format('M d, Y H:i:s'),
          // 'start'       => Carbon::parse($this->start_date_time)->format('M d, Y H:i:s'),
          // 'created_at'  => (string) $this->created_at
        ];
    }
}
;