<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Voyager;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Service;
use App\Models\Subservice;

class PostReviewsResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::where('id',$this->login)->first();
        $profile = asset('storage/upload').$user->profile;
        if($user->profile == ''){
            $profile = asset('storage/upload/images/profile.PNG');
        }
         return [
          'id'            => $this->id,
          'user'          => $user->name,
          'user_profile'  => $profile,
          'rating'         => $this->rating,
          'reviews'         => $this->reviews,
          'created_at'      => $this->created_at,
          'time'    =>     $this->created_at->diffForHumans(),

          // 'title'       => (string) $this->title,
          // 'end'         => Carbon::parse($this->end_date_time)->format('M d, Y H:i:s'),
          // 'start'       => Carbon::parse($this->start_date_time)->format('M d, Y H:i:s'),
          // 'created_at'  => (string) $this->created_at
        ];
    }
}
;