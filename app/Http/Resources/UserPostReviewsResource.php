<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Voyager;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Reviews;
use App\Models\Subservice;

class UserPostReviewsResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $reviewCount = Reviews::select('reviews')->where('post_id',$this->id)->get()->toArray();
        $total_review = count($reviewCount);
        $reviewsSum = Reviews::where('post_id',$this->id)->get()->sum('rating');
        if($total_review != 0){
          $avg = $reviewsSum / $total_review;
        }else{
          $avg = 0;
        }
        
        return [
          'title'  => $this->title,
          'total_review'  => $total_review,
          'rating'  =>(int) round(($avg), 0),
          'review' =>  $reviewCount
        
        ];
    }
}
;