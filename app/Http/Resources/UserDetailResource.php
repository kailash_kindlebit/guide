<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Voyager;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Service;
use App\Models\Subservice;
use App\Models\Location;
use App\Models\Reviews;
use App\Http\Resources\UserResource;

class UserDetailResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::where('id',$this->login)->first();
       
        $user_location = Location::where('id',$user->location)->first();
        $banner_img = asset('storage/upload').$user->banner_img;
        if($user->banner_img == ''){
            $banner_img = asset('storage/upload/images/profile.PNG');
        }
        $profile = asset('storage/upload').$user->profile;
        if($user->profile == ''){
            $profile = asset('storage/upload/images/profile.PNG');
        }

        $imgs = explode(',',$this->image);
        //  print_r($imgs);
        // die;
                $images= [];
                foreach($imgs as $keys=>$img){
                    $images[$keys] = asset('storage/upload/').$img;
        }

        $uimg='';
        if(count($images)!=0){ $uimg = $images[0]; }
        if($uimg == ''){
            $uimg = asset('storage/upload/images/dummy.png');
        }

        $service_name = Service::where('id',$user->category)->first();
        $category = Subservice::where('id',$user->sub_category_id)->first();
        $reviews = Reviews::where('post_id',$this->id)->get();
        $reviewsSum = Reviews::where('post_id',$this->id)->get()->sum('rating');
        $avg = $reviewsSum / 5;
        $totalcomment= count($reviews);
        $reviwes='';
        $reviwesUser = []; 
        if($totalcomment!=0){
            $reviwes =$reviews[0]->reviews;
        }
        $rating = $avg;
        
        //$rating = '4.56';
         return [
          'id'            => $this->id,
          'user'          => $user->name,
          'user_id'          => $user->id,
          'user_phone'          => $user->phone,
          'user_profile'  => $profile,
          'user_location'  => isset($user_location->name_ar)?$user_location->name_ar:null,
          'title'         => $this->title,
          'description'   => $this->description,
          'first_image'         => $uimg,
          'image'         => $images,
          'service_name'  => isset($service_name->name_ar)?$service_name->name_ar:null,
          'category'      => isset($category->name_ar)?$category->name_ar:null,
          'sub_service_image'         =>asset('upload/').'/'.$category->image,
          'service_icon'        =>asset('upload/').'/'.$service_name->image,
          'status'        => $this->status,
          'created_at'    => $this->created_at,
          'time'    =>     $this->created_at->diffForHumans(),
          'review'         => $reviwes,
          'totalcomment'         => $totalcomment,
          'rating'         => (int) round(($rating), 0),

          // 'title'       => (string) $this->title,
          // 'end'         => Carbon::parse($this->end_date_time)->format('M d, Y H:i:s'),
          // 'start'       => Carbon::parse($this->start_date_time)->format('M d, Y H:i:s'),
          // 'created_at'  => (string) $this->created_at
        ];
    }
}
;