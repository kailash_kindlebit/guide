<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Voyager;
use App\Models\User;
use App\Models\Service;
use App\Models\Subservice;
use App\Models\Location;
use App\Models\Post;
use App\Models\Comments;
use App\Http\Resources\PostResource;

class UserNewServiceResource extends JsonResource
{

    public function __construct($resource, $token = '')
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;        
        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $profile = asset('storage/upload').$this->profile;
        if($this->profile == ''){
            $profile = asset('storage/upload/images/profile.PNG');
        }
        $banner_img = asset('storage/upload').$this->banner_img;
        if($this->banner_img == ''){
            $banner_img = asset('storage/upload/images/profile.PNG');
        }
        $service_name = Service::where('id',$this->category)->first();
       
        $sub_service = Subservice::where('id',$this->sub_category_id)->first();
        $location = Location::where('id',$this->location)->first();
        if($request->type == 'ar'){

          $locationName = isset($location->name_ar)?$location->name_ar:null;
          $serviceName =  isset($service_name->name_ar)?$service_name->name_ar:null;
          $subServiceName=isset($sub_service->name_ar)?$sub_service->name_ar:null;
        }else{
          $locationName = isset($location->name)?$location->name:null;
          $serviceName =  isset($service_name->name)?$service_name->name:null;
          $subServiceName=isset($sub_service->name)?$sub_service->name:null;
        }

        $imgs= '';
        $images= [];
        if($this->service_image){
             $imgs = explode(',',$this->service_image);
             if(count($imgs)!=0){
                foreach($imgs as $keys=>$img){
                 $images[$keys] = asset('storage/upload/').$img;
                }
         }
        }
        $postCount = Post::where('login',$this->id)->get();
        $totalPostCount = count($postCount);
        $postCommentCount = Comments::where('login',$this->id)->get();
        $totalPostCommentCount = count($postCommentCount);
        $posts = Post::where('login',$this->id)->orderBy('created_at', 'desc')->limit(5)->get();
        if($request->type == 'ar'){
             $p = UserDetailResource::collection($posts);
        }else{
             $p = UserPostResource::collection($posts);
        }
       

        return [
            'token'               => $this->token,
            'id'                  => $this->id,
            'totalPostCount'             => $totalPostCount,
            'totalPostCommentCount'             => $totalPostCommentCount,
            'username'            => (string) $this->name,
            'email'               => (string) $this->email,
            'phone'               => (string) $this->phone,
            'type'                => $this->type,
            'profile'             => $profile,
            'banner_img'          => $banner_img,
            'yourself'            => $this->yourself,
            'phone'               => $this->phone,
            'service_name'        => $serviceName,
            'service_icon'        =>asset('upload/').'/'.$service_name->image,
            'sub_service'         =>$subServiceName,
            'sub_service_image'         =>asset('upload/').'/'.$sub_service->image,
            'location'            => $locationName,
            'service_image'       => $images,
            'post'       => $p
            
        ];
    }
}
