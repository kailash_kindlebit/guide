<?php

namespace App\Http\Requests;

class RegisterRequest extends ApiRequest
{
    public function rules()
    {
        return [
          'name'      => 'required|string|max:255',
          'email'     => 'required|string|email|max:255|unique:users',
          'password' => 'required|min:6',
          'phone'  => 'required',
          //'service_image'  => 'mimes:jpeg,jpg,png|max:10000',
          'service_name'  => 'required',
          'category'  => 'required',
          'location'  => 'required',
          'city'  => 'required',
          'sub_category_id'  => 'required',
          //'service_document'  => 'required',
        ];
    }
}
