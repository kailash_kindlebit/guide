
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="csrf-token" content="ayCvRBng9QPckujgLhdInyQyXmUbXftGdkPlSZvz">
    <title>Admin  | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    
    <link rel="stylesheet" href="/vendor/laravel-admin/AdminLTE/plugins/iCheck/all.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/AdminLTE/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/flatpickr/dist/shortcut-buttons-flatpickr/themes/light.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/bootstrap-fileinput/css/fileinput.min.css?v=4.5.2">
    <link rel="stylesheet" href="/vendor/laravel-admin/AdminLTE/plugins/select2/select2.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/AdminLTE/plugins/ionslider/ion.rangeSlider.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/AdminLTE/plugins/ionslider/ion.rangeSlider.skinNice.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/fontawesome-iconpicker/dist/css/fontawesome-iconpicker.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/bootstrap-duallistbox/dist/bootstrap-duallistbox.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/AdminLTE/dist/css/skins/skin-blue-light.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/AdminLTE/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/laravel-admin/laravel-admin.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/nprogress/nprogress.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/sweetalert2/dist/sweetalert2.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/nestable/nestable.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/toastr/build/toastr.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/bootstrap3-editable/css/bootstrap-editable.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/google-fonts/fonts.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/AdminLTE/dist/css/AdminLTE.min.css">


    <script src="/vendor/laravel-admin/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="hold-transition skin-blue-light sidebar-mini sidebar-collapse">


<div class="wrapper">

    <!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="/admin" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>Mu</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Murshidcom</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <ul class="nav navbar-nav hidden-sm visible-lg-block">
        
        </ul>

        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <li>
    <a href="javascript:void(0);" class="container-refresh">
        <i class="fa fa-refresh"></i>
    </a>
</li>

                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="/vendor/laravel-admin/AdminLTE/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">Administrator</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="/vendor/laravel-admin/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                            <p>
                                Administrator
                                <small>Member since admin 2022-11-24 07:31:35</small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="/admin/auth/setting" class="btn btn-default btn-flat">Setting</a>
                            </div>
                            <div class="pull-right">
                                <a href="/admin/auth/logout" class="btn btn-default btn-flat">Logout</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                
                    
                
            </ul>
        </div>
    </nav>
</header>
    <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/vendor/laravel-admin/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Administrator</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

                <!-- search form (Optional) -->
        <form class="sidebar-form" style="overflow: initial;" onsubmit="return false;">
            <div class="input-group">
                <input type="text" autocomplete="off" class="form-control autocomplete" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
                <ul class="dropdown-menu" role="menu" style="min-width: 210px;max-height: 300px;overflow: auto;">
                                        <li>
                        <a href="/admin"><i class="fa fa-bar-chart"></i>Dashboard</a>
                    </li>
                                        <li>
                        <a href="/admin/services"><i class="fa fa-briefcase"></i>Services</a>
                    </li>
                                        <li>
                        <a href="/admin/sub-services"><i class="fa fa-bullhorn"></i>Sub Services</a>
                    </li>
                                        <li>
                        <a href="/admin/services-provider"><i class="fa fa-users"></i>Services Provider</a>
                    </li>
                                        <li>
                        <a href="/admin/post"><i class="fa fa-align-right"></i>Post</a>
                    </li>
                                        <li>
                        <a href="/admin/locations"><i class="fa fa-location-arrow"></i>Locations</a>
                    </li>
                                        <li>
                        <a href="/admin/sub-locations"><i class="fa fa-bars"></i>Sub-Locations</a>
                    </li>
                                        <li>
                        <a href="/admin/auth/users"><i class="fa fa-users"></i>Users</a>
                    </li>
                                        <li>
                        <a href="/admin/auth/roles"><i class="fa fa-user"></i>Roles</a>
                    </li>
                                        <li>
                        <a href="/admin/auth/permissions"><i class="fa fa-ban"></i>Permission</a>
                    </li>
                                        <li>
                        <a href="/admin/auth/menu"><i class="fa fa-bars"></i>Menu</a>
                    </li>
                                        <li>
                        <a href="/admin/auth/logs"><i class="fa fa-history"></i>Operation log</a>
                    </li>
                                    </ul>
            </div>
        </form>
        <!-- /.search form -->
        
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Menu</li>

            <li>
                             <a href="/admin">
                            <i class="fa fa-bar-chart"></i>
                                    <span>Dashboard</span>
                            </a>
        </li>
    <li>
                             <a href="/admin/services">
                            <i class="fa fa-briefcase"></i>
                                    <span>Services</span>
                            </a>
        </li>
    <li>
                             <a href="/admin/sub-services">
                            <i class="fa fa-bullhorn"></i>
                                    <span>Sub Services</span>
                            </a>
        </li>
    <li>
                             <a href="/admin/services-provider">
                            <i class="fa fa-users"></i>
                                    <span>Services Provider</span>
                            </a>
        </li>
    <li>
                             <a href="/admin/post">
                            <i class="fa fa-align-right"></i>
                                    <span>Post</span>
                            </a>
        </li>
    <li>
                             <a href="/admin/locations">
                            <i class="fa fa-location-arrow"></i>
                                    <span>Locations</span>
                            </a>
        </li>
    <li>
                             <a href="/admin/sub-locations">
                            <i class="fa fa-bars"></i>
                                    <span>Sub-Locations</span>
                            </a>
        </li>
    <li class="treeview">
            <a href="#">
                <i class="fa fa-tasks"></i>
                                    <span>Admin</span>
                                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                                    <li>
                             <a href="/admin/auth/users">
                            <i class="fa fa-users"></i>
                                    <span>Users</span>
                            </a>
        </li>
                                        <li>
                             <a href="/admin/auth/roles">
                            <i class="fa fa-user"></i>
                                    <span>Roles</span>
                            </a>
        </li>
                                        <li>
                             <a href="/admin/auth/permissions">
                            <i class="fa fa-ban"></i>
                                    <span>Permission</span>
                            </a>
        </li>
                                        <li>
                             <a href="/admin/auth/menu">
                            <i class="fa fa-bars"></i>
                                    <span>Menu</span>
                            </a>
        </li>
                                        <li>
                             <a href="/admin/auth/logs">
                            <i class="fa fa-history"></i>
                                    <span>Operation log</span>
                            </a>
        </li>
                                </ul>
        </li>
    
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
    <div class="content-wrapper" id="pjax-container">
        <style type="text/css"></style>

        <div id="app">
            <section class="content-header">
        <h1>
            Dashboard
            <small>Description...</small>
        </h1>

        <!-- breadcrumb start -->
                <ol class="breadcrumb" style="margin-right: 30px;">
            <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
                    </ol>
        
        <!-- breadcrumb end -->

    </section>

    <section class="content">

         <div class="Doc-sch-lisitng">
            <ul>
                <li>
                    <h2><?php echo $totalserviceprovider ?><span>Total Service Provider's Registration</span></h2>
                </li>
                <li>
                    <h2><?php echo $totaluser ?><span>Total User's Registration</span></h2>
                </li>
                <li>
                    <h2><?php echo $totalpost ?><span>Total Post's</span></h2>
                </li>  
                 <li>
                    <h2><?php echo $totallocation ?><span>Total Location's</span></h2>
                </li>  
                 <li>
                    <h2><?php echo $totalservice ?><span>Total Service's</span></h2>
                </li>  
                 <li>
                    <h2><?php echo $totalsubservice ?><span>Total Sub Location's</span></h2>
                </li>
            </ul>
        </div>
      
        
    </section>
        <style type="text/css">
    .Doc-sch-lisitng ul {
        display: flex;align-items: center;justify-content: start;flex-wrap: wrap;padding: 0;
     }
    .Doc-sch-lisitng ul li {
        width: 30%;background: #fff;height: 150px;margin: 0 14px 20px;display: flex;border: 5px solid #fff;align-items: center;justify-content: start;padding: 15px;border-radius: 10px;box-shadow: 0px 0px 10px #c9c9c9, inset 0 0 10px #c9c9c9;}
    .Doc-sch-lisitng ul li h2 span {font-size: 16px;display: block;font-weight: 500;line-height: 28px;}
    .Doc-sch-lisitng ul li h2 {
        font-size: 42px;font-weight: 700;
    }

    @media(max-width:767px){.Doc-sch-lisitng ul li 
        {
        width: 100%;
        }   
    }
    </style>

        </div>
        <script data-exec-on-popstate>$(function () { ;(function () {
    $('.container-refresh').off('click').on('click', function() {
        $.admin.reload();
        $.admin.toastr.success('Refresh succeeded !', '', {positionClass:"toast-top-center"});
    });
})(); });</script>

        
    </div>

    <!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
                    <strong>Env</strong>&nbsp;&nbsp; local
        
        &nbsp;&nbsp;&nbsp;&nbsp;

                <strong>Version</strong>&nbsp;&nbsp; 1.8.17
        
    </div>
    <!-- Default to the left -->
    <strong>Powered by <a href="https://github.com/z-song/laravel-admin" target="_blank">laravel-admin</a></strong>
</footer>
</div>

<button id="totop" title="Go to top" style="display: none;"><i class="fa fa-chevron-up"></i></button>

<script>
    function LA() {}
    LA.token = "ayCvRBng9QPckujgLhdInyQyXmUbXftGdkPlSZvz";
    LA.user = {"id":1,"username":"admin","name":"Administrator","avatar":"http:\/\/localhost:8000\/vendor\/laravel-admin\/AdminLTE\/dist\/img\/user2-160x160.jpg"};
</script>

<!-- REQUIRED JS SCRIPTS -->
<script src="/vendor/laravel-admin/AdminLTE/bootstrap/js/bootstrap.min.js"></script>
<script src="/vendor/laravel-admin/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="/vendor/laravel-admin/AdminLTE/dist/js/app.min.js"></script>
<script src="/vendor/laravel-admin/jquery-pjax/jquery.pjax.js"></script>
<script src="/vendor/laravel-admin/nprogress/nprogress.js"></script>
<script src="/vendor/laravel-admin/nestable/jquery.nestable.js"></script>
<script src="/vendor/laravel-admin/toastr/build/toastr.min.js"></script>
<script src="/vendor/laravel-admin/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="/vendor/laravel-admin/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="/vendor/laravel-admin/laravel-admin/laravel-admin.js"></script>
<script src="/vendor/laravel-admin/AdminLTE/plugins/iCheck/icheck.min.js"></script>
<script src="/vendor/laravel-admin/AdminLTE/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<script src="/vendor/laravel-admin/AdminLTE/plugins/input-mask/jquery.inputmask.bundle.min.js"></script>
<script src="/vendor/laravel-admin/moment/min/moment-with-locales.min.js"></script>
<script src="/vendor/laravel-admin/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="/vendor/laravel-admin/flatpickr/dist/flatpickr.js"></script>
<script src="/vendor/laravel-admin/flatpickr/dist/shortcut-buttons-flatpickr/shortcut-buttons-flatpickr.min.js"></script>
<script src="/vendor/laravel-admin/flatpickr/dist/l10n/zh.js"></script>
<script src="/vendor/laravel-admin/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
<script src="/vendor/laravel-admin/bootstrap-fileinput/js/fileinput.min.js?v=4.5.2"></script>
<script src="/vendor/laravel-admin/AdminLTE/plugins/select2/select2.full.min.js"></script>
<script src="/vendor/laravel-admin/number-input/bootstrap-number-input.js"></script>
<script src="/vendor/laravel-admin/AdminLTE/plugins/ionslider/ion.rangeSlider.min.js"></script>
<script src="/vendor/laravel-admin/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
<script src="/vendor/laravel-admin/fontawesome-iconpicker/dist/js/fontawesome-iconpicker.min.js"></script>
<script src="/vendor/laravel-admin/bootstrap-fileinput/js/plugins/sortable.min.js?v=4.5.2"></script>
<script src="/vendor/laravel-admin/bootstrap-duallistbox/dist/jquery.bootstrap-duallistbox.min.js"></script>


</body>
</html>
